﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class button : MonoBehaviour
{
    public void Mulai()
    {
        SceneManager.LoadScene("MenuMulai");
    }
    public void Tokoh()
    {
        SceneManager.LoadScene("MenuTokoh");
    }
    public void Sejarah()
    {
        SceneManager.LoadScene("MenuSejarah");
    }
    public void Permainan()
    {
        SceneManager.LoadScene("MenuPermainan");
    }
    public void TentangApp()
    {
        SceneManager.LoadScene("TentangApp");
    }
    public void Gift()
    {
        SceneManager.LoadScene("Gift");
    }
    //Menu Sejarah
    public void Sejarah1()
    {
        SceneManager.LoadScene("Sejarah1");
    }
    public void Sejarah2()
    {
        SceneManager.LoadScene("Sejarah2");
    }
    public void Sejarah3()
    {
        SceneManager.LoadScene("Sejarah3");
    }
    public void Sejarah4()
    {
        SceneManager.LoadScene("Sejarah4");
    }
    public void Sejarah5()
    {
        SceneManager.LoadScene("Sejarah5");
    }

    //Menu Tokoh
    public void Tokoh1()
    {
        SceneManager.LoadScene("Tokoh1");
    }
    public void Tokoh2()
    {
        SceneManager.LoadScene("Tokoh2");
    }
    public void Tokoh3()
    {
        SceneManager.LoadScene("Tokoh3");
    }
    public void Tokoh4()
    {
        SceneManager.LoadScene("Tokoh4");
    }
    public void Tokoh5()
    {
        SceneManager.LoadScene("Tokoh5");
    }

    //Backk
    public void BackMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
    public void BackMenuMulai()
    {
        SceneManager.LoadScene("MenuMulai");
    }
    public void BackDaftarTokoh()
    {
        SceneManager.LoadScene("MenuTokoh");
    }
    public void BackDaftarSejarah()
    {
        SceneManager.LoadScene("MenuSejarah");
    }
    public void Keluar()
    {
        Application.Quit();
    }
}

